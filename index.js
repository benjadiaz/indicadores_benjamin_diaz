const descargarDatos = require('./datos');
const archivos = require('./archivos');
const est = require('./estadisticas');
const readLine = require('readline');
//require('log-timestamp');

var lector = readLine.createInterface(
    {
        input: process.stdin,
        output: process.stdout
    }
);

function actualizarDatos() {
    descargarDatos().then(archivos.escribir).then(() => menu(), error => {
        console.error("No se puede conectar al servidor. Volviendo al menú. " + error);
        menu();
    });
};

function calcularPromedio() {
    archivos.leerTodos().then(est.promedio, console.error).then(console.log).then(() => menu())
}

function ultimoValor() {
    archivos.leerUltimo().then(console.log, console.error).then(() => menu())
}

function valorMinimo() {
    archivos.leerTodos().then(est.valoresMinimos).then(datos => {
        console.log("Rango: " + new Date(datos.fechaInicio).toLocaleDateString() + " a " + new Date(datos.fechaFin).toLocaleDateString());
        console.log("Dólar a " + datos.dolar.valor + "$ el " + datos.dolar.fecha);
        console.log("Euro a " + datos.euro.valor + "$ el " + datos.euro.fecha);
        console.log("Tasa desempleo a " + datos.tasa_desempleo.valor + "% el " + datos.tasa_desempleo.fecha);
    }).then(() => menu())
};

function valorMaximo() {
    archivos.leerTodos().then(est.valoresMaximos).then(datos => {
        console.log("Rango: " + new Date(datos.fechaInicio).toLocaleDateString() + " a " + new Date(datos.fechaFin).toLocaleDateString());
        console.log("Dólar a " + datos.dolar.valor + " el " + datos.dolar.fecha);
        console.log("Euro a " + datos.euro.valor + " el " + datos.euro.fecha);
        console.log("Tasa desempleo a " + datos.tasa_desempleo.valor + "% el " + datos.tasa_desempleo.fecha);
    }).then(() => menu())
}

function menu() {
    console.log('\n-------------------------------------');
    console.log('MENU');
    console.log('1. Actualizar indicadores');
    console.log('2. Promedio indicadores');
    console.log('3. Mostrar último valor');
    console.log('4. Mostrar mínimo histórico');
    console.log('5. Mostrar máximo histórico');
    console.log('6. Salir');
    lector.question('Escriba su opción\n', opcion => {
        switch (opcion) {
            case '1': actualizarDatos(); break;
            case '2': calcularPromedio(); break;
            case '3': ultimoValor(); break;
            case '4': valorMinimo(); break;
            case '5': valorMaximo(); break;
            case '6':
                lector.close();
                process.exit(0);
                break;
            default:
                console.log("Error: Ingresa un número del 1 al 6");
                menu();
                break;
        }
        console.log('');
    });
};

menu();