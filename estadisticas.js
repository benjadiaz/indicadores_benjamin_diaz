const archivos = require('./archivos');
var fechas = {};

function calcularPromedio(datos) {
    return new Promise(function (resolve, reject) {
        if (!datos) {
            reject("Error: No hay datos.");
            return;
        }
        let prom = {
            "Promedio dólar": obtenerPromedio("dolar", datos),
            "Promedio euro": obtenerPromedio("euro", datos),
            "Tasa de desempleo promedio": obtenerPromedio("tasa_desempleo", datos)
        };
        resolve(prom);
    });
}

function obtenerPromedio(indice, datos) {
    let suma = 0;
    datos.forEach(elemento => suma += elemento[indice].valor);
    return (suma / datos.length);
}

function valorMinimo(datos) {
    return new Promise(async function (resolve, reject) {
        if (!datos || datos.length == 0) {
            reject("Error: No hay datos.");
            return;
        }

        let min = {
            "dolar": await obtenerMinimo("dolar", datos),
            "euro": await obtenerMinimo("euro", datos),
            "tasa_desempleo": await obtenerMinimo("tasa_desempleo", datos),
            "fechaInicio": fechas.inicio,
            "fechaFin": fechas.fin,
        };
        resolve(min);


    });

}

function obtenerMinimo(indice, datos) {
    return new Promise(function (resolve, reject) {
        let minimo = (datos[0][indice]);
        if (!fechas.inicio) fechas.inicio = minimo.fecha;
        if (!fechas.fin) fechas.fin = minimo.fecha;
        datos.forEach(elemento => {
            const e = elemento[indice]
            if (e.fecha > fechas.fin) fechas.fin = e.fecha;
            if (e.fecha < fechas.inicio) fechas.inicio = e.fecha;
            if (e.valor < minimo.valor) {
                minimo = {
                    valor: e.valor,
                    fecha: e.fecha
                };
            }
        });
        resolve(minimo);
    })
}

function valorMaximo(datos) {
    return new Promise(async function (resolve, reject) {
        if (!datos) {
            reject("Error: No hay datos.");
            return;
        }
        let max = {
            "dolar": await obtenerMaximo("dolar", datos),
            "euro": await obtenerMaximo("euro", datos),
            "tasa_desempleo": await obtenerMaximo("tasa_desempleo", datos),
            "fechaInicio": fechas.inicio,
            "fechaFin": fechas.fin,
        }
        resolve(max);
    });

}

function obtenerMaximo(indice, datos) {
    return new Promise(function (resolve, reject) {
        let maximo = (datos[0][indice]);
        if (!fechas.inicio) fechas.inicio = maximo.fecha;
        if (!fechas.fin) fechas.fin = maximo.fecha;
        datos.forEach(elemento => {
            const e = elemento[indice]
            if (e.fecha > fechas.fin) fechas.fin = e.fecha;
            if (e.fecha < fechas.inicio) fechas.inicio = e.fecha;
            if (e.valor > maximo.valor) {
                maximo = {
                    valor: e.valor,
                    fecha: e.fecha
                };
            }
        });
        resolve(maximo);
    });
}

module.exports = {
    promedio: calcularPromedio,
    valoresMinimos: valorMinimo,
    valoresMaximos: valorMaximo
}