const fs = require('fs');

function escribir(datos) {
    let date = new Date();
    datos =
    {
        dolar: datos.dolar,
        euro: datos.euro,
        tasa_desempleo: datos.tasa_desempleo
    }
    fs.writeFile(('./datos/' + date.getFullYear() + ((date.getMonth() >= 10) ? date.getMonth() : ('0' + date.getMonth())) + ((date.getDay() >= 10) ? date.getDay() : ('0' + date.getDay())) + ((date.getHours() >= 10) ? date.getHours() : ('0' + date.getHours())) + ((date.getMinutes() >= 10) ? date.getMinutes() : ('0' + date.getMinutes())) + ((date.getSeconds() >= 10) ? date.getSeconds() : ('0' + date.getSeconds())) + '.ind'), JSON.stringify(datos), function (err) {
        if (err) throw err;
    });
}

const promesaLectura = nombreArchivo => {
    return new Promise((resolve, reject) => {
        fs.readFile(nombreArchivo, (err, data) => {
            if (err) {
                console.error(err);
                reject(err);
                return;
            }
            resolve(data);
        })
    });
}

function leerTodos() {
    return new Promise(function (resolve, reject) {
        fs.readdir('./datos', { withFileTypes: false }, function (err, archivos) {
            if (err) {
                reject('No se pudo encontrar los archivos.');
                return;
            }
            if (archivos[0] == '.DS_Store') archivos.splice(0,1);
            const arr = (archivos.map(archivo => {
                return promesaLectura("./datos/" + archivo).then(JSON.parse);
            }));
            Promise.all(arr).then(resolve);
        });
    });
};

function leerUltimo() {
    return new Promise(function (resolve, reject) {
        fs.readdir('./datos', { withFileTypes: false }, function (err, archivos) {
            if (archivos[0] == '.DS_Store') archivos.splice(0, 1);
            const arrOrdenado = archivos.map(function(e)
            {
                return parseInt(e);
            }).sort(((a,b) => {
                return a-b;
            }));
            if (archivos.length > 0)
                resolve(promesaLectura('./datos/' + (arrOrdenado[archivos.length - 1] + ".ind")).then(JSON.parse).catch(console.error));
            else reject("No existen archivos.");
        });
    });
}

module.exports =
    {
        escribir: escribir,
        leerTodos: leerTodos,
        leerUltimo: leerUltimo
    }